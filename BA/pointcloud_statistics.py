import numpy as np
import math as m
from itertools import count
import matplotlib.pyplot as plt

file = r"H:\Photogrammetrie\2022\BA Master\CloudCompare\M3C2 Fuji vs. iPad Photogrammetry.txt"
# file = r"H:\Photogrammetrie\2022\BA Master\CloudCompare\Test_short.txt"

m3c2_distance = np.empty((1,))
i = 0
with open(file) as f:
    while (line := f.readline().rstrip()):
        i += 1
        if i != 1 and i != 2 and i != 3:
            line = np.fromstring(line, sep='\t')
            m3c2_distance = np.append(m3c2_distance, line[9])
            print(i)

m3c2_distance = m3c2_distance[~np.isnan(m3c2_distance)]
m3c2_distance = m3c2_distance[1:]

n, bins, patches = plt.hist(m3c2_distance, 200, density=True, facecolor='g', alpha=0.75)
plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
plt.grid(True)
plt.show()